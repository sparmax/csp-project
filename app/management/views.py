from django.shortcuts import render
from django.views import View

from competitions.models import Competition, CompetitionCategory
from applications.models import Application


# TODO: admin required decorator
class ManagementMainView(View):
    template_name = 'management_main.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class ManagementCompetitionListView(View):
    template_name = 'management_competition_list.html'

    def get(self, request, *args, **kwargs):
        # TODO
        return render(request, self.template_name, context=dict(
            competitions=Competition.objects.all(),
            competition_categories=CompetitionCategory.objects.all(),
        ))


class ManagementApplicationListView(View):
    template_name = 'management_application_list.html'

    def get(self, request, *args, **kwargs):
        # TODO
        return render(request, self.template_name, context=dict(
            applications=Application.objects.select_related('competition').all()
        ))
