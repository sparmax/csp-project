from django.urls import path
from .views import \
    ManagementMainView, ManagementCompetitionListView, ManagementApplicationListView

urlpatterns = [
    path('management/main', ManagementMainView.as_view(), name='management_main_view'),
    path('management/competitions', ManagementCompetitionListView.as_view(), name='management_competitions_view'),
    path('management/applications', ManagementApplicationListView.as_view(), name='management_applications_view'),
]
