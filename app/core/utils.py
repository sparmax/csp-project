from functools import partial

from django.contrib.auth.decorators import login_required

login_required_without_redirect = partial(login_required, redirect_field_name=None)

