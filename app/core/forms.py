from django import forms


class ClearableFileInput(forms.ClearableFileInput):
    template_name = 'clearable_file_input.html'

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self.model = kwargs.pop('model', None)

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        # if self.model:
        #     context['widget'].update({
        #         'file_name': self.model.name,
        #         'file_url': self.model.file.name
        #     })
        # context = super().get_context(name, value, attrs)
        # checkbox_name = self.clear_checkbox_name(name)
        # checkbox_id = self.clear_checkbox_id(checkbox_name)
        # context['widget'].update({
        #     'checkbox_name': checkbox_name,
        #     'checkbox_id': checkbox_id,
        #     'is_initial': self.is_initial(value),
        #     'input_text': self.input_text,
        #     'initial_text': self.initial_text,
        #     'clear_checkbox_label': self.clear_checkbox_label,
        # })
        return context
