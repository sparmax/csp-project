import os
from uuid import uuid4

from django.db import models

from csp.settings import MEDIA_URL, MEDIA_ROOT


class SoftDeletionQuerySet(models.QuerySet):
    def delete(self):
        return super(SoftDeletionQuerySet, self).update(is_deleted=True)

    def hard_delete(self):
        return super(SoftDeletionQuerySet, self).delete()


class SoftDeletionManager(models.Manager):
    def __init__(self, *args, **kwargs):
        self.alive_only = kwargs.pop('alive_only', True)
        super(SoftDeletionManager, self).__init__(*args, **kwargs)

    def get_queryset(self):
        if self.alive_only:
            return SoftDeletionQuerySet(self.model).filter(is_deleted=False)
        return SoftDeletionQuerySet(self.model)

    def hard_delete(self):
        return self.get_queryset().hard_delete()


class SoftDeletionModel(models.Model):
    class Meta:
        abstract = True

    def delete(self):
        self.is_deleted = True
        self.save()

    def hard_delete(self):
        super(SoftDeletionModel, self).delete()


def func_upload_to(path):
    def upload_to(instance, filename):
        _, file_ext = os.path.splitext(filename)
        return os.path.join(path, instance.document_id, instance.uuid.hex, file_ext)
    return upload_to


class DocumentFile(models.Model):
    UPLOAD_DIRNAME = ""
    UPLOAD_PATH = os.path.join(MEDIA_ROOT, UPLOAD_DIRNAME)

    id = models.AutoField(primary_key=True)
    uuid = models.UUIDField(default=uuid4, editable=False)
    name = models.CharField(max_length=255, null=False)
    file = models.FileField(upload_to=func_upload_to(UPLOAD_PATH), db_column='filepath')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    @property
    def document_id(self):
        raise NotImplementedError

    @property
    def absolute_url(self):
        _, file_ext = os.path.splitext(self.file.name)
        return "{media}{dir}/{id}/{hex}{ext}".format(
            media=MEDIA_URL, dir=self.UPLOAD_DIRNAME, id=self.document_id, hex=self.uuid.hex, ext=file_ext
        )
