from django.urls import path
from .views import CompetitionListView, CompetitionView

urlpatterns = [
    path('competitions/', CompetitionListView.as_view(), name='competitions_view'),
    path('competitions/<int:competition_id>/<slug:slug>/', CompetitionView.as_view(), name='competition_view')
]
