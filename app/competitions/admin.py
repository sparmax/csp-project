from django.contrib import admin
from django import forms

from ckeditor.widgets import CKEditorWidget

from core.forms import ClearableFileInput

from .models import Competition, CompetitionCategory, CompetitionFile


class CompetitionAdminForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'style': 'width: 480px'}))
    full_description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Competition
        exclude = ('user', 'is_deleted',)


class CompetitionFileAdminForm(forms.ModelForm):
    # file = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
    file = forms.FileField(widget=ClearableFileInput())

    class Meta:
        model = CompetitionFile
        fields = ('file', 'name')
        

class CompetitionFileInline(admin.TabularInline):
    model = CompetitionFile
    form = CompetitionFileAdminForm
    extra = 1
    readonly_fields = ('created_at',)
    can_delete = True


class CompetitionAdmin(admin.ModelAdmin):
    inlines = (
        CompetitionFileInline,
    )

    list_display = (
        'name', 'category', 'user',
        'start_time', 'end_time', 'created_at', 'updated_at',
        'is_deleted',
    )

    search_fields = ('name',)
    form = CompetitionAdminForm

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        print("ojb.name=", obj.name)
        super().save_model(request, obj, form, change)

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            print("INSTANCE:", instance.id, "CHANGE:", change)
            instance.user = request.user
            instance.save()


class CompetitionCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_at', 'updated_at', 'is_deleted',)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super().save_model(request, obj, form, change)


admin.site.register(Competition, CompetitionAdmin)
admin.site.register(CompetitionCategory, CompetitionCategoryAdmin)
