from django.db import models
from django.utils.functional import cached_property

from slugify import slugify

from csp.settings import MEDIA_COMPETITION_FILES_DIRNAME

from accounts.models import User
from core.models import SoftDeletionManager, DocumentFile


class CompetitionCategoriesManager(SoftDeletionManager):
    pass


class CompetitionCategory(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'competition_categories'

    objects = CompetitionCategoriesManager()
    all_objects = CompetitionCategoriesManager(alive_only=False)

    def __str__(self):
        return self.name

    @cached_property
    def slug(self):
        return self.name.replace(' ', '-')


class CompetitionManager(SoftDeletionManager):
    pass


class Competition(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, null=False, on_delete=models.DO_NOTHING)
    category = models.ForeignKey(CompetitionCategory, null=False, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, null=False)
    brief_description = models.CharField(max_length=1024)
    full_description = models.TextField()
    start_time = models.DateTimeField(null=False)
    end_time = models.DateTimeField(null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'competitions'

    objects = CompetitionManager()
    all_objects = CompetitionManager(alive_only=False)

    def __str__(self):
        return self.name

    @cached_property
    def slug(self):
        return slugify(self.name)


class CompetitionFile(DocumentFile):
    UPLOAD_DIRNAME = MEDIA_COMPETITION_FILES_DIRNAME

    user = models.ForeignKey(User, null=False, on_delete=models.DO_NOTHING)
    competition = models.ForeignKey(Competition, on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'competition_files'

    @property
    def document_id(self):
        return self.competition.id


# class CompetitionFile(models.Model):
#     UPLOAD_PATH = os.path.join(MEDIA_ROOT, MEDIA_COMPETITION_FILES_DIRNAME)

#     id = models.AutoField(primary_key=True)
#     user = models.ForeignKey(User, null=False, on_delete=models.DO_NOTHING)
#     competition = models.ForeignKey(Competition, on_delete=models.DO_NOTHING)
#     uuid = models.UUIDField(default=uuid4, editable=False)
#     name = models.CharField(max_length=255, null=False)
#     file = models.FileField(
#         upload_to=func_upload_to(UPLOAD_PATH, ),
#         db_column='filepath',
#     )
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)
#     is_deleted = models.BooleanField(default=False)

#     class Meta:
#         db_table = 'competition_files'

#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         # self.file.fname = self.name
#         # self.file.name = self.relative_url  # render in widget html as url

#     def __str__(self):
#         return self.name

#     def save(self, *args, **kwargs):
#         # self.name = self.file.name
#         self.name = self.file.name
#         super().save(*args, **kwargs)

#     # def delete(self, *args, **kwargs):
#     #     print("DELETE")

#     @property
#     def url(self):
#         return self.file.url

#     # @property
#     # def relative_url(self):
#     #     _, file_ext = os.path.splitext(self.file.name)
#     #     return "{}/{}/{}".format(MEDIA_COMPETITION_FILES_DIRNAME, self.uuid.hex, file_ext)
