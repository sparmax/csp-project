from django.utils.decorators import method_decorator
from django.shortcuts import render, redirect
from django.views import View

from core.utils import login_required_without_redirect

from .models import Competition, CompetitionCategory, CompetitionFile


@method_decorator(login_required_without_redirect, name='dispatch')
class CompetitionListView(View):
    template_name = 'competition_list.html'

    def get(self, request, *args, **kwargs):
        # ctx = Context({
        #     'competitions': Competition.objects.all(),
        #     'competition_categories': CompetitionCategory.objects.all()
        # })
        return render(request, self.template_name, context=dict(
            competitions=Competition.objects.all(),
            competition_categories=CompetitionCategory.objects.all(),
        ))


class CompetitionView(View):
    template_name = 'competition.html'

    def get(self, request, *args, **kwargs):
        competition_id = kwargs.pop('competition_id', None)

        competition = Competition.objects.get(pk=competition_id)
        if competition is None:
            pass  # TODO

        competition_files = CompetitionFile.objects.filter(competition_id=competition_id).all()

        return render(request, self.template_name, context=dict(
            competition=competition,
            competition_files=competition_files
        ))

    def post(self, request, *args, **kwargs):
        competition_id = kwargs.pop('competition_id', None)

        competition = Competition.objects.get(pk=competition_id)
        if competition is None:
            pass  # TODO

        # Application.objects.create(request.user.id, competition_id)

        return redirect('competition_view', competition_id)
