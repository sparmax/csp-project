template = {
    'text_input': 'text_input.html',
    'text_area': 'text_area.html',
    'ckeditor_widget': 'ckeditor_widget.html',
    'select_dropdown': 'select_dropdown.html',
}


class Field:
    def __init__(self, id, name, type, is_required, pos, description=None, context=None):
        self.id = id
        self.name = name
        self.type = type
        self.is_required = is_required
        self.pos = pos
        self.description = description
        self.context = context


class FieldValue:
    def __init__(self, id, value):
        self.id = id
        self.value = value
