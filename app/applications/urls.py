from django.urls import path
from .views import \
    ApplicationListView, ApplicationView, ApplicationCommentListView, \
    ApplicationFieldsetView, ApplicationMemberListView, MemberListAjaxView

urlpatterns = [
    path('applications', ApplicationListView.as_view(), name='applications_view'),
    path('applications/<int:application_id>/', ApplicationView.as_view(), name='application_view'),
    path('applications/<int:application_id>/forms/<int:fieldset_id>/<slug:slug>/', ApplicationFieldsetView.as_view(), name='application_fieldset_view'),
    path('applications/<int:application_id>/comments', ApplicationCommentListView.as_view(), name='application_comments_view'),
    path('applications/<int:application_id>/members', ApplicationMemberListView.as_view(), name='application_members_view'),
    path('members', MemberListAjaxView.as_view(), name='members_ajax_view'),
]
 