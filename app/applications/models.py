from enum import Enum

from django.db import models, transaction
from django.contrib.postgres.fields import JSONField
from django.utils.functional import cached_property

from slugify import slugify

from accounts.models import User
from competitions.models import Competition


class ApplicationMemberManager(models.Manager):
    def get_role(self, user_id, application_id):

        member = models.QuerySet(self.model).filter(user_id=user_id, application_id=application_id).only('role').first()
        return member.role if member else None


class ApplicationMember(models.Model):
    class Role:
        AUTHOR = 'author'
        COAUTHOR = 'coauthor'
        CURATOR = 'curator'

        @classmethod
        def has_value(cls, v):
            return v in (cls.AUTHOR, cls.COAUTHOR, cls.CURATOR)

    ROLE_CHOICES = (
        (Role.AUTHOR, 'Автор'),
        (Role.COAUTHOR, 'Соавтор'),
        (Role.CURATOR, 'Куратор'),
    )

    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, null=False, on_delete=models.DO_NOTHING)
    application = models.ForeignKey('Application', null=False, on_delete=models.DO_NOTHING)
    role = models.CharField(max_length=32, choices=ROLE_CHOICES)
    read_only = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'application_members'
        ordering = ['role']

    objects = ApplicationMemberManager()

    # TODO: replace to dict
    @property
    def role_display(self):
        return next((r[1] for r in self.ROLE_CHOICES if r[0] == self.role), None)


class ApplicationManager(models.Manager):
    def create(self, user_id, competition_id):
        try:
            with transaction.atomic():
                application = self.model(competition_id=competition_id, status='on_filling')
                application.save()
                member = ApplicationMember(user=user_id, application_id=application.id, status='author')
                member.save()
                return application
        except Exception as e:
            raise e  # TODO

    def get_author(self, application_id):
        return ApplicationMember.objects.filter(application_id=application_id, role='author').select_related('User').first()

    def get_coauthors(self, application_id):
        return ApplicationMember.objects.filter(application_id=application_id, role='coauthor').select_related('User').all()

    def get_curators(self, application_id):
        return ApplicationMember.objects.filter(application_id=application_id, role='curator').select_related('User').all()

    def get_members(self, application_id):
        return ApplicationMember.objects.filter(application_id=application_id).select_related('User').all()


class Application(models.Model):
    STATUS_CHOICES = (
        ('on_filling', 'На заполнении'),
        ('on_moderation', 'На модерации'),
        ('on_correction', 'На исправлении'),
        ('approved', 'Принято'),
        ('rejected', 'Отклонено')
    )

    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, null=False, on_delete=models.DO_NOTHING)
    competition = models.ForeignKey(Competition, null=False, on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=255)
    status = models.CharField(max_length=32, choices=STATUS_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'applications'

    objects = ApplicationManager()

    def author(self):
        return self.objects.get_author(self.id)

    def coauthors(self):
        return self.objects.get_coauthors(self.id)

    def curators(self):
        return self.objects.get_curators(self.id)

    def members(self):
        return self.objects.get_members(self.id)

    @cached_property
    def slug(self):
        return slugify(self.name)

    # TODO: replace to dict
    @property
    def status_display(self):
        return next((r[1] for r in self.STATUS_CHOICES if r[0] == self.status), None)


class ApplicationFieldsetCategory(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, null=False, on_delete=models.DO_NOTHING)
    competition = models.ForeignKey(Competition, null=False, on_delete=models.DO_NOTHING)
    parent = models.ForeignKey('self', null=True, on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=255)
    level = models.SmallIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'application_fieldset_categories'
        ordering = ['level']

    def __str__(self):
        return self.name

    @cached_property
    def slug(self):
        return slugify(self.name)


class ApplicationFieldsetValueManager(models.Manager):
    def update_or_create_some(self, user_id, fieldset_id, values):
        return self.model.objects.update_or_create(
            user_id=user_id,
            fieldset_id=fieldset_id,
            defaults={'content': dict(fields=values)}
        )


class ApplicationFieldsetValue(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, null=False, on_delete=models.DO_NOTHING)
    fieldset = models.ForeignKey('ApplicationFieldset', null=False, on_delete=models.DO_NOTHING)
    content = JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'application_fieldset_values'

    objects = ApplicationFieldsetValueManager()


class ApplicationFieldsetManager(models.Manager):
    def get_fields_with_value(self, user_id, fieldset_id):
        fieldset = self.model.objects.get(pk=fieldset_id)
        if fieldset is None:
            pass  # TODO

        result_fields = dict()
        for field in fieldset.content['fields']:
            k = field.get('id', 0)
            result_fields[k] = field

        fieldset_value = ApplicationFieldsetValue.objects.filter(fieldset_id=fieldset.id, user_id=user_id).first()
        if fieldset_value is not None:
            for field in fieldset_value.content['fields']:
                k = field.get('id', 0)
                result_fields[k]['value'] = field['value']

        return sorted(result_fields.values(), key=lambda v: v['pos'])


class ApplicationFieldset(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, null=False, on_delete=models.DO_NOTHING)
    category = models.ForeignKey(ApplicationFieldsetCategory, null=False, on_delete=models.DO_NOTHING)
    content = JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    # TODO create column in table
    icon = 'glyphicon glyphicon-tasks'

    class Meta:
        db_table = 'application_fieldsets'
        

    objects = ApplicationFieldsetManager()


class ApplicationComment(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, null=False, on_delete=models.DO_NOTHING)
    application = models.ForeignKey(Application, null=False, on_delete=models.DO_NOTHING)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'application_comments'
        ordering = ['created_at']
