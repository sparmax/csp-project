from django.utils.decorators import method_decorator
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views import View
from django.http import JsonResponse

from core.utils import login_required_without_redirect

from .models import \
    Application, ApplicationFieldset, ApplicationFieldsetCategory, \
    ApplicationFieldsetValue, ApplicationComment, ApplicationMember

from .forms import CommentForm


@method_decorator(login_required_without_redirect, name='dispatch')
class ApplicationView(View):
    template_name = 'application.html'

    def get(self, request, *args, **kwargs):
        application_id = kwargs.pop('application_id', None)

        application = Application.objects.get(pk=application_id)
        if application is None:
            pass  # TODO

        # fieldset_categories = ApplicationFieldsetCategory.objects.filter(competition_id=application.competition.id).all()
        fieldsets = ApplicationFieldset.objects.defer('content').filter(category__competition_id=application.competition.id).select_related('category')

        # TODO: refactor
        fieldsets = list(fieldsets)
        fieldsets.sort(key=lambda f: f.category.level)

        return render(request, self.template_name, context=dict(
            application=application,
            # fieldset_categories=fieldset_categories,
            fieldsets=fieldsets,
            comments_amount=ApplicationComment.objects.filter(application_id=application_id).count()
        ))

    def post(self, request, *args, **kwargs):
        competition_id = request.POST.get('competition_id', None)
        if competition_id is None:
            pass  # TODO

        application = Application.objects.create(competition_id=competition_id, name="", status='on_filling')

        return redirect('application_view', application_id=application.id)


@method_decorator(login_required_without_redirect, name='dispatch')
class ApplicationListView(View):
    template_name = 'application_list.html'

    def get(self, request, *args, **kwargs):
        pass


@method_decorator(login_required_without_redirect, name='dispatch')
class ApplicationFieldsetView(View):
    template_name = 'application_fieldset.html'

    def get(self, request, *args, **kwargs):
        application_id = kwargs.pop('application_id', None)
        fieldset_id = kwargs.pop('fieldset_id', None)

        application = Application.objects.select_related('user').get(pk=application_id)
        if application is None:
            pass  # TODO

        fieldsets = ApplicationFieldset.objects.defer('content').filter(category__competition_id=application.competition.id).select_related('category')
        #fieldset_categories = ApplicationFieldsetCategory.objects.filter(competition_id=application.competition.id).all()

        # category_id = next((category.id for category in fieldset_categories if category.level == category_level), None)
        # if category_id is None:
        #     pass  # TODO

        form_fields = ApplicationFieldset.objects.get_fields_with_value(application.user.id, fieldset_id)
        
        # TODO: refactor
        fieldsets = list(fieldsets)
        fieldsets.sort(key=lambda f: f.category.level)

        return render(request, self.template_name, context=dict(
            application=application,
            fieldsets=fieldsets,
            current_category=next((f.category for f in fieldsets if f.id == fieldset_id), ""),
            form_fields=form_fields,
            comments_amount=ApplicationComment.objects.filter(application_id=application_id).count()
        ))

    def post(self, request, *args, **kwargs):
        application_id = kwargs.pop('application_id', None)
        fieldset_id = kwargs.pop('fieldset_id', None)

        application = Application.objects.select_related('user').get(pk=application_id)
        if application is None:
            pass  # TODO

        form_data = request.POST.dict()
        form_data.pop('csrfmiddlewaretoken')

        values = []
        for k, v in form_data.items():
            values.append(dict(id=int(k), value=v))

        ApplicationFieldsetValue.objects.update_or_create_some(application.user.id, fieldset_id, values)

        messages.success(request, "Форма успешно сохранена")

        # return redirect('application_fieldset_view', application_id, category_level, slug)
        return redirect(request.path)


@method_decorator(login_required_without_redirect, name='dispatch')
class ApplicationCommentListView(View):
    template_name = 'application_comments.html'

    def get(self, request, *args, **kwargs):
        application_id = kwargs.pop('application_id', None)

        application = Application.objects.get(pk=application_id)
        if application is None:
            pass  # TODO

        comments = ApplicationComment.objects.filter(application_id=application_id).select_related('user')

        fieldsets = ApplicationFieldset.objects.defer('content').filter(category__competition_id=application.competition.id).select_related('category')

        # TODO: refactor
        fieldsets = list(fieldsets)
        fieldsets.sort(key=lambda f: f.category.level)

        return render(request, self.template_name, context=dict(
            application=application,
            comments=comments,
            fieldsets=fieldsets,
            comments_amount=len(comments)
        ))

    def post(self, request, *args, **kwargs):
        application_id = kwargs.pop('application_id', None)
        application = Application.objects.select_related('user').get(pk=application_id)
        if application is None:
            pass  # TODO

        form = CommentForm(request.POST)
        if form.is_valid():
            form_data = form.cleaned_data
            ApplicationComment.objects.create(
                user_id=application.user.id, application_id=application_id, text=form_data['comment_text']
            )
            messages.success(request, "Комментарий успешно добавлен")
        else:
            messages.error(request, "Не удалось создать новый комментарий")

        return redirect(request.path)


@method_decorator(login_required_without_redirect, name='dispatch')
class ApplicationMemberListView(View):
    template_name = 'application_members.html'

    def get(self, request, *args, **kwargs):
        application_id = kwargs.pop('application_id', None)

        application = Application.objects.get(pk=application_id)
        if application is None:
            pass  # TODO

        members = ApplicationMember.objects.filter(application_id=application_id).select_related('user')
        fieldsets = ApplicationFieldset.objects.defer('content').filter(category__competition_id=application.competition.id).select_related('category')

        # TODO: refactor
        fieldsets = list(fieldsets)
        fieldsets.sort(key=lambda f: f.category.level)

        return render(request, self.template_name, context=dict(
            application=application,
            members=members,
            fieldsets=fieldsets,
            is_author=next((m for m in members if m.user.id == request.user.id and m.role == m.Role.AUTHOR), False),  # TODO: refactor
            comments_amount=ApplicationComment.objects.filter(application_id=application_id).count()
        ))

    def post(self, request, *args, **kwargs):
        member_id = request.POST.get('member_id')

        if member_id is None:
            pass  # TODO

        ApplicationMember.objects.filter(id=member_id).delete()

        messages.success(request, "Пользователь успешно удален из списка участников")
        return redirect(request.path)


@method_decorator(login_required_without_redirect, name='dispatch')
class MemberListAjaxView(View):
    def post(self, request, *args, **kwargs):
        user_id = request.POST.get('user_id')
        application_id = request.POST.get('application_id')
        select_role = request.POST.get('role')

        current_role = ApplicationMember.objects.get_role(request.user.id, application_id)
        if current_role is None or current_role != ApplicationMember.Role.AUTHOR:
            # TODO: need message
            print("role=", current_role, application_id, request.user.id)
            return JsonResponse(dict(success=False), status=403)
        
        if not ApplicationMember.Role.has_value(select_role):
            # TODO: need message
            return JsonResponse(dict(success=False), status=400)

        member, created = ApplicationMember.objects.get_or_create(user_id=user_id, application_id=application_id, defaults={'role': select_role})
        if not created:
            messages.error(request, "Ошибка при добавлении, пользователь уже в списке участников", extra_tags='danger')
            return JsonResponse(dict(success=False), status=400)

        messages.success(request, "Пользователь успешно добавлен в список участников")
        return JsonResponse(dict(success=True))
