"""csp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from django.conf.urls.static import static

from csp import settings

from accounts import urls as accounts_urls
from home import urls as home_urls
from competitions import urls as competitions_urls
from applications import urls as applications_urls
from management import urls as management_urls


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(accounts_urls)),
    path('', include(home_urls)),
    path('', include(competitions_urls)),
    path('', include(applications_urls)),
    path('', include(management_urls)),
]

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)