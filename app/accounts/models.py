from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser


class UserManager(BaseUserManager):
    def create_user(self, email, password, **fields):
        user = self.model(email=email, **fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **fields):
        raise NotImplementedError


class User(AbstractBaseUser):
    id = models.AutoField(primary_key=True)
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    patronymic = models.CharField(max_length=255)
    date_joined = models.DateTimeField(auto_now=True, db_column='created_at')
    last_login = models.DateTimeField(auto_now=True, db_column='last_login_at')
    is_staff = True
    
    def has_module_perms(*args, **kwargs):
        return True

    def has_perm(*args, **kwargs):
        return True

    class Meta:
        db_table = 'users'

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'patronymic']

    @property
    def full_name(self):
        return "{0.last_name} {0.first_name} {0.patronymic}".format(self)

    @property
    def short_name(self):
        return "{0.last_name} {0.first_name[0]}.{0.patronymic[0]}.".format(self)

    def __str__(self):
        return self.email

    def get_full_name(self):
        """For django compatibility."""
        return self.full_name

    def get_short_name(self):
        """For django compatibility."""
        return self.short_name

