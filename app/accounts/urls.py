from django.urls import path
from .views import LoginView, RegistrationView, LogoutView, UserListAjaxView

urlpatterns = [
    path('login/', LoginView.as_view(), name='login_view'),
    path('registration/', RegistrationView.as_view(), name='registration_view'),
    path('logout/', LogoutView.as_view(), name='logout_view'),
    path('users', UserListAjaxView.as_view(), name='users_ajax_view'),
]
