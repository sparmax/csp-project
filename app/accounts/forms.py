from django import forms


class LoginForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField()


class RegistrationForm(forms.Form):
    email = forms.EmailField()
    first_name = forms.CharField(label='first_name')
    last_name = forms.CharField(label='last_name')
    patronymic = forms.CharField(label='patronymic')
    password = forms.CharField()
