from django.utils.decorators import method_decorator
from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth import login, logout, authenticate
from django.db import IntegrityError
from django.http import JsonResponse

from core.utils import login_required_without_redirect

from .forms import LoginForm, RegistrationForm

from .models import User


class LoginView(View):
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST)
        if not form.is_valid():
            # DEBUG
            print("errors=", form.errors)
            raise Exception("FORM IS NOT VALID")
        print("data = ", form.data)
        data = form.cleaned_data
        user = authenticate(
            request,
            username=data['email'],
            password=data['password']
        )
        if user is None:
            raise Exception("USER NOT EXISTS")

        login(request, user)
        return redirect('home_view')


class RegistrationView(View):
    template_name = 'registration.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        form = RegistrationForm(request.POST)
        if not form.is_valid():
            # DEBUG
            print("errors=", form.errors)
            raise Exception("FORM IS NOT VALID")

        data = form.cleaned_data
        try:
            user = User.objects.create_user(
                email=data['email'],
                password=data['password'],
                first_name=data['first_name'],
                last_name=data['last_name'],
                patronymic=data['patronymic'],
            )
        except IntegrityError as e:
            raise Exception("EMAIL ALREADY EXISTS")
        except Exception as e:
            # DEBUG
            raise e

        login(request, user)
        return redirect('home_view')


@method_decorator(login_required_without_redirect, name='dispatch')
class LogoutView(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect('home_view')


@method_decorator(login_required_without_redirect, name='dispatch')
class UserListAjaxView(View):
    def get(self, request, *args, **kwargs):
        params = request.GET.get('name', '').split(' ')

        # TODO: it's for css animations testing, remove it
        import time
        time.sleep(0.5)

        if len(params) == 0:
            pass  # TODO

        last_name, first_name, *_ = params if len(params) > 1 else params[0], " " # noqa

        # TODO: add search by first_name too
        users = User.objects.\
            only('id', 'first_name', 'last_name', 'patronymic').\
            filter(last_name__istartswith=last_name)
        response = [dict(id=u.id, full_name=u.full_name) for u in users]  # TODO: need json marshalling in the model
        return JsonResponse(response, safe=False)
