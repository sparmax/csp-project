from django.utils.decorators import method_decorator
from django.shortcuts import render
from django.views import View

from core.utils import login_required_without_redirect


@method_decorator(login_required_without_redirect, name='dispatch')
class HomeView(View):
    template_name = 'home.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)
