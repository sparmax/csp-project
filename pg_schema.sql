CREATE TABLE IF NOT EXISTS users (
	id SERIAL NOT NULL PRIMARY KEY,
	email varchar(255) NOT NULL UNIQUE,
	password varchar(255) NOT NULL,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255) NOT NULL,
	patronymic VARCHAR(255) NOT NULL,
	is_admin BOOLEAN NOT NULL DEFAULT false,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	last_login_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS invites (
	id SERIAL NOT NULL PRIMARY KEY,
	email varchar(255) NOT NULL,\
	application_id,
	role member_role,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
)

CREATE TYPE project_status AS ENUM('on_filling', 'on_moderation', 'approved', 'rejected');

CREATE TABLE IF NOT EXISTS projects (
	id SERIAL NOT NULL PRIMARY KEY,
	name varchar(255) NOT NULL,
	status project_status NOT NULL DEFAULT 'on_filling',
	description TEXT NOT NULL DEFAULT "",
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	is_deleted BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE IF NOT EXISTS project_comments (
	id SERIAL NOT NULL PRIMARY KEY,
	user_id INT NOT NULL,
	project_id INT NOT NULL,
	text TEXT NOT NULL,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	is_deleted BOOLEAN NOT NULL DEFAULT false,
	FOREIGN KEY(user_id) REFERENCES users(id),
	FOREIGN KEY(project_id) REFERENCES projects(id)
);

CREATE TABLE IF NOT EXISTS competition_categories (
	id SERIAL NOT NULL PRIMARY KEY,
	user_id INT NOT NULL,
	name varchar(255) NOT NULL,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	is_deleted BOOLEAN NOT NULL DEFAULT false
	FOREIGN KEY(user_id) REFERENCES users(id),
);

INSERT INTO competition_categories(name) VALUES
('Научно-технический'),
('Гуманитарный');

CREATE TABLE IF NOT EXISTS competitions (
	id SERIAL NOT NULL PRIMARY KEY,
	user_id INT NOT NULL,
	category_id INT NOT NULL,
	name varchar(255) NOT NULL,
	brief_description varchar(1024) NOT NULL DEFAULT "",
	full_description TEXT NOT NULL DEFAULT "",
	start_time TIMESTAMPTZ NOT NULL,
	end_time TIMESTAMPTZ NOT NULL,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	is_deleted BOOLEAN NOT NULL DEFAULT false,
	FOREIGN KEY(user_id) REFERENCES users(id),
	FOREIGN KEY(category_id) REFERENCES competition_categories(id)
);

CREATE TYPE application_status AS ENUM('on_filling', 'on_moderation', 'on_correction', 'approved', 'rejected');

CREATE TABLE IF NOT EXISTS applications (
	id SERIAL NOT NULL PRIMARY KEY,
	user_id INT NOT NULL,
	competition_id INT NOT NULL,
	name varchar(255) NOT NULL,
	status application_status NOT NULL DEFAULT 'on_filling',
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	is_deleted BOOLEAN NOT NULL DEFAULT false,
	FOREIGN KEY(user_id) REFERENCES users(id),
	FOREIGN KEY(competition_id) REFERENCES competitions(id)
);

-- CREATE TABLE IF NOT EXISTS application_base_categories (
-- 	id SERIAL NOT NULL PRIMARY KEY,
-- 	user_id INT NOT NULL,
-- 	competition_id INT NOT NULL,
-- 	name varchar(255) NOT NULL,
-- 	level SMALLINT NOT NULL,
-- 	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
-- 	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
-- 	is_deleted BOOLEAN NOT NULL DEFAULT false,
-- 	FOREIGN KEY(user_id) REFERENCES users(id),
-- 	FOREIGN KEY(competition_id) REFERENCES competitions(id),
-- );

CREATE TABLE IF NOT EXISTS application_fieldset_categories (
	id SERIAL NOT NULL PRIMARY KEY,
	user_id INT NOT NULL,
	competition_id INT NOT NULL,
	parent_id INT DEFAULT NULL,
	name varchar(255) NOT NULL,
	level SMALLINT NOT NULL,
	-- brief_description varchar(1024),
	-- full_description TEXT,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	is_deleted BOOLEAN NOT NULL DEFAULT false,
	FOREIGN KEY(user_id) REFERENCES users(id),
	FOREIGN KEY(competition_id) REFERENCES competitions(id),
	FOREIGN KEY(parent_id) REFERENCES application_fieldset_categories(id)
);

CREATE TABLE IF NOT EXISTS application_fieldsets (
	id SERIAL NOT NULL PRIMARY KEY,
	user_id INT NOT NULL,
	category_id INT NOT NULL,
	content JSONB NOT NULL,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	is_deleted BOOLEAN NOT NULL DEFAULT false,
	FOREIGN KEY(user_id) REFERENCES users(id),
	FOREIGN KEY(category_id) REFERENCES application_fieldset_categories(id)
);

CREATE TYPE member_role AS ENUM('author', 'coauthor', 'curator');

CREATE TABLE IF NOT EXISTS application_members (
	id SERIAL NOT NULL PRIMARY KEY,
	user_id INT NOT NULL,
	application_id INT NOT NULL,
	role member_role NOT NULL,
	read_only BOOLEAN NOT NULL DEFAULT false,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	FOREIGN KEY(user_id) REFERENCES users(id),
	FOREIGN KEY(application_id) REFERENCES applications(id)
);

CREATE TABLE IF NOT EXISTS application_fieldset_values (
	id SERIAL NOT NULL PRIMARY KEY,
	user_id INT NOT NULL,
	fieldset_id INT NOT NULL,
	content JSONB NOT NULL,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	is_deleted BOOLEAN NOT NULL DEFAULT false,
	FOREIGN KEY(user_id) REFERENCES users(id),
	FOREIGN KEY(fieldset_id) REFERENCES application_fieldsets(id)
);

CREATE TABLE IF NOT EXISTS application_comments (
	id SERIAL NOT NULL PRIMARY KEY,
	user_id INT NOT NULL,
	application_id INT NOT NULL,
	text TEXT NOT NULL,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	is_deleted BOOLEAN NOT NULL DEFAULT false,
	FOREIGN KEY(user_id) REFERENCES users(id),
	FOREIGN KEY(application_id) REFERENCES applications(id)
);

CREATE TABLE IF NOT EXISTS application_files (
	id SERIAL NOT NULL PRIMARY KEY,
	user_id INT NOT NULL,
	application_id INT NOT NULL,
	uuid UUID NOT NULL,
	name VARCHAR(255) NOT NULL,
	filepath VARCHAR(4096) NOT NULL,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	is_deleted BOOLEAN NOT NULL DEFAULT false,
	FOREIGN KEY(user_id) REFERENCES users(id),
	FOREIGN KEY(application_id) REFERENCES applications(id)
);

CREATE TABLE IF NOT EXISTS competition_files (
	id SERIAL NOT NULL PRIMARY KEY,
	user_id INT NOT NULL,
	competition_id INT NOT NULL,
	uuid UUID NOT NULL,
	name VARCHAR(255) NOT NULL,
	filepath VARCHAR(4096) NOT NULL,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	is_deleted BOOLEAN NOT NULL DEFAULT false,
	FOREIGN KEY(user_id) REFERENCES users(id),
	FOREIGN KEY(competition_id) REFERENCES competitions(id)
);

CREATE TABLE IF NOT EXISTS project_files (
	id SERIAL NOT NULL PRIMARY KEY,
	user_id INT NOT NULL,
	project_id INT NOT NULL,
	uuid UUID NOT NULL,
	name VARCHAR(255) NOT NULL,
	filepath VARCHAR(4096) NOT NULL,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(), 
	is_deleted BOOLEAN NOT NULL DEFAULT false,
	FOREIGN KEY(user_id) REFERENCES users(id),
	FOREIGN KEY(project_id) REFERENCES projects(id)
);

CREATE TABLE notifications (
	id SERIAL NOT NULL PRIMARY KEY,
	user_id INT NOT NULL,
	content JSON NOT NULL,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	is_readed BOOLEAN NOT NULL DEFAULT false,
	FOREIGN KEY(user_id) REFERENCES users(id)
);